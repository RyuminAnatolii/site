<?php
// Підключення файлу, що містить клас для роботи з базою даних
include_once 'db_connect.php';

// Створення екземпляру класу Database і передача параметрів для підключення до бази даних
$database = new Database('localhost', 'root', '', 'repaircars');

try {
    // Виконання запиту до бази даних на отримання списку співробітників
    $result = $database->query("SELECT * FROM employees");

    if ($result) {
        // Отримання даних про співробітників з результатів запиту
        $employees = $result->fetch_all(MYSQLI_ASSOC);
    } else {
        // Обробка помилки, якщо запит не виконався успішно
        echo "Помилка запиту до бази даних: " . mysqli_error($database->getConnection());
    }
} catch(Exception $e) {
    // Обробка винятку, якщо сталася помилка при виконанні запиту до бази даних
    echo 'Помилка запиту до бази даних: ' . $e->getMessage();
}
?>
