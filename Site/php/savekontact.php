<?php
include_once 'db_connect.php'; // Підключення до файлу з функцією підключення до бази даних
$database = new Database('localhost', 'root', '', 'repaircars'); // Створення об'єкту бази даних з параметрами підключення

if ($_SERVER['REQUEST_METHOD'] === 'POST') { // Перевірка, чи метод запиту POST
  $name = $_POST['name']; // Отримання даних з форми
  $email = $_POST['email'];
  $message = $_POST['message'];

  try {
    $result = $database->query("INSERT INTO contacts (name, email, message) VALUES ('$name', '$email', '$message')"); // Виконання запиту на додавання даних в таблицю contacts
    if ($result) { // Якщо запит виконано успішно
      $message = "Ваше повідомлення успішно надіслано.";
    } else { // Якщо сталась помилка виконання запиту
      $message = "Помилка запиту до бази даних: " . mysqli_error($database->getConnection()); // Формування повідомлення про помилку
    }
  } catch(Exception $e) { // Якщо сталась помилка виконання запиту
    $message = 'Помилка запиту до бази даних: ' . $e->getMessage(); // Формування повідомлення про помилку
  }
}

header("Location: index.php?message=" . urlencode($message)); // Перенаправлення на головну сторінку з повідомленням про результат дії
?>