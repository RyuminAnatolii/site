<?php
include_once 'db_connect.php'; // Підключення до бази даних
$database = new Database('localhost', 'root', '', 'repaircars'); // Створення об'єкту бази даних

$message = ''; // Ініціалізація змінної для зберігання повідомлення про результат дії

// Перевірка методу запиту до сервера
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  $name = $_POST['name'];
  $description = $_POST['description'];

  try {
    // Виконання запиту до бази даних
    $result = $database->query("INSERT INTO reviews (name, description) VALUES ('$name', '$description')");

    // Перевірка на успішність виконання запиту
    if ($result) {
      $message = "Дякуємо! Ваш відгук успішно відправлений.";
    } else {
      $message = "Помилка запиту до бази даних: " . mysqli_error($database->getConnection());
    }
  } catch(Exception $e) {
    $message = 'Помилка запиту до бази даних: ' . $e->getMessage();
  }
}

header("Location: index.php?message=" . urlencode($message)); // Перенаправлення на головну сторінку з повідомленням про результат дії
?>
