<?php
class Database {
    private $connectionPool;
    
    // Конструктор класу, приймає параметри для підключення до бази даних
    public function __construct($host, $username, $password, $database) {
        // Створення об'єкту mysqli і зберігання його у властивості connectionPool
        $this->connectionPool = new \mysqli($host, $username, $password, $database);

        // Обробка помилки, якщо підключення до бази даних не вдалося
        if ($this->connectionPool->connect_error) {
            throw new Exception('Database connection failed: ' . $this->connectionPool->connect_error);
        }
    }
    
    // Метод для виконання запиту до бази даних
    public function query($sql) {
        // Виконання запиту і збереження результату у змінну $result
        $result = $this->connectionPool->query($sql);

        // Обробка помилки, якщо запит не вдалося виконати
        if (!$result) {
            throw new Exception('Database query error: ' . $this->connectionPool->error);
        }

        // Повернення результату запиту
        return $result;
    }
}
?>
