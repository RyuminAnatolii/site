<?php
include_once 'db_connect.php'; // підключення до бази даних
$database = new Database('localhost', 'root', '', 'repaircars'); // створення об'єкту бази даних

try {
    $result = $database->query("SELECT * FROM reviews"); // виконання запиту до бази даних на вибірку усіх записів з таблиці відгуків
    if ($result) {
        $reviews = $result->fetch_all(MYSQLI_ASSOC); // збереження результату в змінну у вигляді асоціативного масиву
    } else {
        echo "Помилка запиту до бази даних: " . mysqli_error($database->getConnection()); // виведення повідомлення про помилку в разі невдачі виконання запиту
    }
} catch(Exception $e) {
    echo 'Помилка запиту до бази даних: ' . $e->getMessage(); // виведення повідомлення про помилку в разі виникнення виключення
}
?>
