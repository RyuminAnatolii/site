<?php
include_once 'db_connect.php'; // Підключення до файлу, що містить код для підключення до бази даних
$database = new Database('localhost', 'root', '', 'repaircars'); // Створення об'єкту бази даних з параметрами хосту, користувача, паролю та назви бази даних

try {
    $result = $database->query("SELECT * FROM services"); // Виконання запиту до бази даних для отримання всіх записів з таблиці "services"
    if ($result) {
        $services = $result->fetch_all(MYSQLI_ASSOC); // Отримання результату запиту в масиві асоціативних масивів, який містить дані про всі послуги
    } else {
        echo "Помилка запиту до бази даних: " . mysqli_error($database->getConnection()); // Виведення повідомлення про помилку, якщо запит не було успішно виконано
    }
} catch(Exception $e) {
    echo 'Помилка запиту до бази даних: ' . $e->getMessage(); // Виведення повідомлення про помилку, якщо сталася помилка в процесі виконання запиту
}

?>
