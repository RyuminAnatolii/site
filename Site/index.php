<!DOCTYPE html>
<html lang="en">

<head> <!-- Відкриваючий тег для розділу документу "head" -->
    <meta charset="UTF-8"> <!-- Встановлюю кодування сторінки -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> <!-- Встановлююю параметри для масштабування сторінки на мобільних пристроях -->
    <meta http-equiv="X-UA-Compatible" content="ie=edge"> <!-- Встановлююююв параметри для відображення сторінки в Internet Explorer -->
    <title>Назва вашої компанії</title> <!-- Встановлююю заголовок сторінки -->
    <!-- Підключення CSS-файлів Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
        crossorigin="anonymous">
    <!-- Підключення вашого власного CSS-файлу -->
    <link rel="stylesheet" href="css/style.css"> <!-- Підключення власного CSS-файлу -->
</head> <!-- Закриваючий тег для розділу документу "head" -->
<body> <!-- Відкриваючий тег для розділу документу "body" -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light"> <!-- Відкриваючий тег для навігаційного блоку -->
  <div class="container d-flex justify-content-center"> <!-- Відкриваючий тег для контейнера з позиціонуванням по центру -->
    <a class="navbar-brand" href="#"> <!-- Посилання на головну сторінку сайту -->
      <img src="images/logo.jpg" alt="logo.jpg" style="max-height: 180px;"> <!-- Логотип сайту -->
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
      aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation"> <!-- Кнопка для розкриття меню на маленьких екранах -->
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-center" id="navbarNav"> <!-- Відкриваючий тег для розкриваємого меню -->
      <ul class="navbar-nav"> <!-- Відкриваючий тег для списку навігації -->
        <li class="nav-item"> <!-- Пункт меню -->
          <a class="nav-link" href="#about-us">Про нас</a> <!-- Посилання на сторінку "Про нас" -->
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#services">Послуги</a> <!-- посилання на сторінку зі списком послуг -->
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#employees">Працівники</a> <!-- посилання на сторінку зі списком працівників -->
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#review">Відгуки</a> <!-- посилання на сторінку з відгуками -->
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#contacts">Контакти</a> <!-- посилання на сторінку з контактною інформацією -->
        </li>
      </ul>
    </div>
  </div>
</nav>
<!-- Заголовок -->
<header class="jumbotron jumbotron-fluid">
  <div class="container" id="about-us">
    <h1 class="display-4">Вітаємо на сайті нашої компанії AUTO REPAIR!</h1> <!-- заголовок сторінки -->
    <p class="lead">Ми займаємося ремонтом та обслуговуванням легкових автомобілів</p> <!-- підзаголовок сторінки -->
    <a href="#contacts" class="btn btn-primary btn-lg">Дізнатися більше</a> <!-- кнопка, що має перенаправляти на сторінку з додатковою інформацією (тимчасово перенаправляє на контакти) -->
  </div>
</header>

    <!-- Розмітка для відображення списку послуг -->
<div class="container" id="services">
  <h2 class="text-center my-5">Наші послуги</h2>
  <div class="row">
    <?php include 'php/services.php'; ?> <!-- Підключення файлу зі списком послуг -->
    <?php foreach($services as $service): ?> <!-- Цикл для відображення кожної послуги -->
    <div class="col-md-4">
      <div class="card">
        <img src="<?php echo $service['image_url']; ?>" class="card-img-top" alt="..." style="height: 200px; object-fit: cover;">
        <div class="card-body">
          <h5 class="card-title"><?php echo $service['title']; ?></h5>
          <p class="card-text" style="height: 170px;"><?php echo $service['description']; ?></p>
        </div>
      </div>
    </div>
    <?php endforeach; ?>
  </div>
</div>

<!-- Розмітка для відображення списку працівників -->
<div class="container" id="employees">
  <h2 class="text-center my-5">Наші працівники</h2>
  <div class="row">
  <?php include 'php/employees.php'; ?> <!-- Підключення файлу зі списком працівників -->
  <?php foreach($employees as $employee): ?> <!-- Цикл для відображення кожного працівника -->
  <div class="col-md-4">
    <div class="card">
      <img src="<?php echo $employee['image_url']; ?>" class="card-img-top" alt="..." style="height: 200px; object-fit: cover;">
      <div class="card-body">
        <h5 class="card-title"><?php echo $employee['name']; ?></h5>
        <p class="card-text"><?php echo $employee['position']; ?></p>
        <p class="card-text"><?php echo $employee['description']; ?></p>
      </div>
    </div>
  </div>
  <?php endforeach; ?>
</div>


<!-- Блок з виводом відгуків -->

<div class="container my-5" id="review">
  <h2 class="text-center text-black mb-4">Відгуки</h2>
  <!-- Вивід блоків з відгуками, використовуючи php скрипт -->
  <div class="row overflow-auto">
    <?php include 'php/reviews.php'; ?>
    <?php foreach ($reviews as $review): ?>
      <div class="col-md-4 col-lg-3 mb-4">
        <div class="card shadow-sm h-100">
          <!-- Блок з відгуком -->
          <div class="card-body">
            <h5 class="card-title text-primary"><?php echo $review['name']; ?></h5>
            <p class="card-text"><?php echo $review['description']; ?></p>
          </div>
        </div>
      </div>
    <?php endforeach; ?>
  </div>
</div>
<!-- Блок для написання відгуку -->
<div class="container" >
  <h2>Створити відгук</h2>
  <!-- Форма для відправлення нового відгуку на сервер, використовуючи php скрипт -->
  <form action="php/insert_review.php" method="POST">
    <div class="form-group">
      <label for="name">Ім'я:</label>
      <input type="text" class="form-control" id="name" name="name" required>
    </div>
    <div class="form-group">
      <label for="description">Відгук:</label>
      <textarea class="form-control" id="description" name="description" rows="5" required></textarea>
    </div>
    <!-- Кнопка для відправки форми на сервер -->
    <button type="submit" class="btn btn-primary">Відправити</button>
  </form>
</div>



<section id="contacts" >
  <!-- Розділ для контактної інформації та форми зворотнього зв'язку -->
  <div class="container">
    <!-- Контейнер для зберігання контенту -->
    <div class="row">
      <!-- Рядок для розміщення контенту в колонках -->
      <div class="col-md-6">
        <!-- Колонка для відображення контактної інформації -->
        <h3>Контактна інформація</h3>
        <p>Наша компанія знаходиться за адресою: вул. Міцкевича, 123, м. Коломия</p>
        <p>Телефон: +380123456789</p>
        <p>Email: info@example.com</p>
      </div>
      <div class="col-md-6">
        <!-- Колонка для форми зворотнього зв'язку -->
        <h3>Форма зворотнього зв'язку</h3>
        <form method="post" action="php/savekontact.php">
          <!-- Форма для відправки повідомлення -->
          <div class="form-group">
            <!-- Група для введення імені -->
            <label for="name">Ім'я</label>
            <input type="text" class="form-control" id="name" name="name" placeholder="Введіть ім'я">
          </div>
          <div class="form-group">
            <!-- Група для введення електронної пошти -->
            <label for="email">Email</label>
            <input type="email" class="form-control" id="email" name="email" placeholder="Введіть email">
          </div>
          <div class="form-group">
            <!-- Група для введення повідомлення -->
            <label for="message">Повідомлення</label>
            <textarea class="form-control" id="message" name="message" rows="5" placeholder="Введіть повідомлення"></textarea>
          </div>
          <button type="submit" class="btn btn-primary">Надіслати</button>
        </form>
      </div>
    </div>
  </div>
</section>

</body>
